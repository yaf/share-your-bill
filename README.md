Share your bills
================

Le but est d'avoir une application capable de stocker des informations issu de mes tickets de courses... 

Dans un premier temps, c'est histoire de voir le prix de ce que j'achete, et socker tout ça quelque part, pouvoir faire des analyse dessus, des comparaisons...

Ce readme à pour but de noter mes idées, envies par rapport à ce projet. C'est un peu à la façon de ce billet http://tom.preston-werner.com/2010/08/23/readme-driven-development.html

- Pas de gestion d'utilisateurs (je veux pas faire le flickage de ce que j'ai, ou ce que d'autres on acheté)
- affichage clair de ce que me coute l'application (en temps et en ressource matériel)
- Reconnaissance de charactère pour pouvoir prendre un ticket de caisse en photo/scan et juste valider le tout avant stockage (il faut que l'on puisse mettre des infos très vite et très simplement)
- Ajouter le lieux, le magasins (marque) et la date
...

(temps passé à l'init du projet: 5 minutes...)


note
----

*Skerou*

On dirais qu'une application assez proche (en tout cas, contenant une partie de ce que je souhaite faire ici) existe 
déjà: http://skerou.com/. 

A ceci prêt que je ne souhaite pas monter une boite et toucher des sous, de la même manière que je n'ai pas envie de faire un systèmes d'authentification. Mais la partie photo du ticket, reconnaissance des caractères et persistance en vue de pouvoir faire des requêtes dessus, ça, c'est ce que je veux :-)


*OpenFoodFacts*

Ca serais bien de se brancher là dessus http://fr.openfoodfacts.org/. Il faut voir comment (techniquement) et comment au niveau données...