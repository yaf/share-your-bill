require 'sinatra'

set :views, File.join(settings.root,'..', 'views')
set :haml, :format => :html5

class Bill
  def self.create_form
  end
end

get '/' do
  haml :index
end

get '/bill/new' do
  haml :new_bill
end

post '/bill' do
  puts "params #{params}"
  Bill.create_from(params)
  redirect url('/bill/new')
end

