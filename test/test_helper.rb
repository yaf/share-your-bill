require './lib/share_your_bill'
require 'test/unit'
require 'rack/test'
require 'mocha'

set :environment, :test
set :run, false
set :raise_errors, true
set :logging, false
