require 'test_helper'

ENV['RACK_ENV'] = 'test'

class BillCreationTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def test_it_says_hello_world
    get '/'
    assert last_response.ok?
  end

  def test_it_show_bill_creation_form
    get '/bill/new'
    assert last_response.ok?
  end

  def test_it_respond_to_post_bill
    Bill.expects(:create_from).with({})
    post '/bill'
    assert last_response.redirect?
  end
end
